package com.myfirstapp2;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

import com.myfirstapp2.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;;

public class GrantButton extends ImageButton {
	
	public class TaintCleaner{
		public String clearString(String str){
			return str + " cleaned!";
		}
	}
	
	
	public GrantButton(Context context, AttributeSet attrs) {
		super(context);
		final TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.GrantButton, 0, 0);
		this.setImageResource(R.drawable.gps_to_internet);
		this.setOnClickListener(new OnClickListener(){

			 private Method mHandler;
			 private String handlerName = a.getString(R.styleable.GrantButton_onGrant);
			 @Override
             public void onClick(View v) {
                 if (mHandler == null) {
                     try {
                         mHandler = getContext().getClass().getMethod(handlerName,
                        		 View.class, Object.class, Method.class);
                     } catch (NoSuchMethodException e) {
                         int id = getId();
                         String idText = id == NO_ID ? "" : " with id '"
                                 + getContext().getResources().getResourceEntryName(
                                     id) + "'";
                         throw new IllegalStateException("Could not find a method " +
                                 handlerName + "(View, Object, Method) in the activity "
                                 + getContext().getClass() + " for onClick handler"
                                 + " on view " + GrantButton.this.getClass() + idText, e);
                     }
                 }

                 try {
                	 TaintCleaner cleaner = new TaintCleaner();
                     mHandler.invoke(getContext(), (GrantButton.this), cleaner, cleaner.getClass().getMethod("clearString", String.class));
                 } catch (IllegalAccessException e) {
                     throw new IllegalStateException("Could not execute non "
                             + "public method of the activity", e);
                 } catch (InvocationTargetException e) {
                     throw new IllegalStateException("Could not execute "
                             + "method of the activity", e);
                 } catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
             }
			
		});
	}
	 
}
