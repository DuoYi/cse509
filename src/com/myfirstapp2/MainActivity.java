package com.myfirstapp2;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;

import com.myfirstapp2.R;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		try {
			udp = new DatagramSocket();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private List<String> log = new LinkedList<String>();
	
	public void logStatus(String stat){
		if(log.size() > 5){
			log.remove(0);
		}
		log.add(stat);
		TextView tv = (TextView) findViewById(R.id.textView3);
		StringBuilder sb = new StringBuilder();
		for(String s : log){
			sb.append(s + '\n');
		}
		tv.setText(sb.toString());
	}
	DatagramSocket udp = null;
	
	private void sendUdp(String msg){
		EditText addr = (EditText) findViewById(R.id.editText2);
		EditText port = (EditText) findViewById(R.id.editText3);
		Integer p = Integer.parseInt(port.getText().toString());
		ClientThread thread = new ClientThread(addr.getText().toString(),p,msg);
		new Thread(thread).start();
	}
	
	public void sendMessage(View view){
		EditText txt = (EditText) findViewById(R.id.editText1);
		String msg = txt.getText().toString();
		sendUdp(msg);
		txt.setText("");
	}
	
	public void sendLocation(View view){
		if(MyApplication.currentLocation == null){
			logStatus("Could not get location");
			return;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("Longtitude=");
		sb.append(MyApplication.currentLocation.getLongitude());
		sb.append("; Latitude=");
		sb.append(MyApplication.currentLocation.getLatitude());
		String msg = sb.toString();
		sendUdp(msg);
	}
	public void sendGrantedLocation(View view, Object obj, Method m){
		try {
			this.logStatus(m.invoke(obj, "hello world").toString());
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	class ClientThread implements Runnable {
		String addr;
		int port;
		String data;
		public ClientThread(String a, int p, String d){
			addr = a;
			port = p;
			data = d;
		}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				byte[] b = ('>'+data).getBytes();
				DatagramPacket pkt = new DatagramPacket(b, b.length, InetAddress.getByName(addr), port);
				udp.send(pkt);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				data = e.getLocalizedMessage();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				data = e.getLocalizedMessage();
			} finally{
				runOnUiThread(new Runnable(){
					@Override
					public void run() {
						logStatus(data);
					}
				});
			}
		}

	}
}
